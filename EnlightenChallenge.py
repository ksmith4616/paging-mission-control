import sys
from datetime import datetime
import json


def main():
    #Ensure data file is only argument
    if len(sys.argv) == 1:
        sys.exit("Please enter data file as argument.   Ex: python3 EnlightenChallenge.py data.txt")
    elif len(sys.argv) > 2:
        sys.exit("Please enter only one data file at a time.   Ex: python3 EnlightenChallenge.py data.txt")
        
    #Pass data file over to parse and return alerts found
    batt_alerts, tstat_alerts = parse_data(sys.argv[1])

    #Generate alerts based on parsed data
    notify_user = generate_alerts(tstat_alerts, "RED HIGH", "TSTAT") + generate_alerts(batt_alerts, "RED LOW", "BATT")

    #Print alerts according to correct format
    print(json.dumps(notify_user, indent=4))


def parse_data(data_file):
    batt_alerts, tstat_alerts = {}, {}
    
    try:
        with open(data_file) as file:
            for row in file:
                #Assign variables for easier readability (yellow as well for future use) & ensure data present in row
                data = row.strip().split("|")
                if len(data) == 8:
                    time, sat_id, red_high, yel_high, yel_low, red_low, raw_value, component = data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]
                
                    #If data meets alert threshold, store in specified alerts dictionary
                    if component == "TSTAT" and (float(raw_value) > float(red_high)):
                        if sat_id not in tstat_alerts:
                            tstat_alerts[sat_id] = [time]
                        else:
                            tstat_alerts[sat_id] += [time]                
                    elif component == "BATT" and (float(raw_value) < float(red_low)):
                        if sat_id not in batt_alerts:
                            batt_alerts[sat_id] = [time]
                        else:
                            batt_alerts[sat_id] += [time]
    except FileNotFoundError:
        sys.exit("File does not exist. Please enter valid file")

    return batt_alerts, tstat_alerts

def generate_alerts(alerts, severity, component):
    date_format = '%Y%m%d %H:%M:%S.%f'
    alert_output = []

    for id in alerts:
        #Ignore satellite id if there are fewer than 3 alerts
        if len(alerts[id]) >= 3:
            first_alert, third_alert = 0, 2
            while third_alert < len(alerts[id]):
                start, end = datetime.strptime(alerts[id][first_alert], date_format), datetime.strptime(alerts[id][third_alert], date_format)

                #Check to see if alerts are within 5 min time interval, if so add to alert_output.
                if (end - start).seconds/60 <= 5:
                    alert_output.append({'satelliteId': int(id), 'severity': severity, 'component': component, 'timestamp': start.isoformat(timespec='milliseconds')+'Z' })

                #Check if there are enough alerts left to notify on
                #If there are move forward to not have alerts overlap within 5 minute timeframe
                if len(alerts[id][third_alert:]) > 2:
                    for x in alerts[id][(third_alert + 1):]:
                        if (datetime.strptime(x, date_format) - start).seconds/60 >= 5:
                            first_alert = alerts[id].index(x)
                            third_alert = first_alert + 2
                            break      
                else:
                    break
                            
    return alert_output
           

if __name__ == "__main__":
    main()